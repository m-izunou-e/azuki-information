<h3 id="init-azuki">Azukiパッケージの初期設定を行う</h3>
<h4>環境の確認</h4>
<div class="section-block">
  <p>
    環境面にて以下が設定されていることが前提です
    <ul>
      <li>Web経由にてLaravelのページに正しくアクセス出来ていること</li>
      <li>本システムで使用するDBが構築されていること</li>
      <li>DBへの接続設定が.envにて出来ていること</li>
      <li>.envにてメールに関する設定が出来ていること</li>
    </ul>
  </p>

  <h5>ロケーションの設定</h3>
  <p class="margin-bottom-0">
    <code>config/app.php</code>にて
  </p>
  <pre><code class="prettyprint linenums block">
    'timezone'        => 'Asia/Tokyo',
    'locale'          => 'ja',
  </code></pre>
  <p>
    と設定を変更します。
  </p>

</div>

<h4>初期設定</h4>
<div class="section-block">
  <p>
    初期設定の基本的な手順は以下になります。
    <ul>
      <li>publicディレクトリにCSS・JSファイルを出力します。</li>
      <li>DBのマイグレーションを行います。</li>
      <li>SEEDにて初期データを投入します。</li>
      <li>日本語の言語ファイルを設定します。</li>
    </ul>
  </p>
    
  <h5>CSS・JSファイルの出力</h5>
  <p>
    <p class="command">
      $ php artisan vendor:publish --provider="Azuki\ServiceProvider" --tag=public
    </p>
    を実行します。
  </p>
    
  <h5>マイグレーションの実行</h5>
  <p>
    まず初めにLaravelにて登録されているmigrationファイルを実行しないように削除または移動してください。
    例）
    <p class="command">
      $ mv database/migrations database/migrations_default
    </p>
    など。<br>
    その後&nbsp;migration&nbsp;を実行します。
    <p class="command">
      $ php artisan migrate
    </p>
    migrate&nbsp;に失敗した場合はエラーメッセージを元に調整してください。
  </p>

  <h5>SEEDによる初期データ投入</h5>
  <p>
    <p class="command">
      $ php artisan vendor:publish --provider="Azuki\ServiceProvider" --tag=seed
    </p>
    を実行します。<br>
    database/seeders&nbsp;に&nbsp;seeder&nbsp;ファイルが出力されます。
    <p class="annotation">
      Azukiバージョンによっては、&nbsp;database/seeds&nbsp;になります。
    </p>
    <p>
      <code>DirectorsTableSeeder.php</code><br>
      {{--<code>ManagersTableSeeder.php</code><br>--}}
      をエディタで開き、ユーザー名、パスワードを変更してください。
    </p>
    <p class="command">
      $ composer dump-autoload
    </p>
    を実行し、seederファイルを実行できるようにします。
    <p class="annotation">
      laravel8.x以上の場合は上記のaump-autoloadの作業は不要です
    </p>
    <p class="command">
      $ php artisan db:seed --class=MasterDataSeeder<br>
      $ php artisan db:seed --class=DirectorsTableSeeder<br>
      {{--$ php artisan db:seed --class=ManagersTableSeeder<br>--}}
    </p>
    を実行します。
  </p>

  <h5>エラーメッセージの日本語化</h3>
  <p class="margin-bottom-0">
    この時点ではエラーメッセージなどがの日本語化されていません。
  </p>
  <p class="command">
    $ php artisan vendor:publish --provider="Azuki\ServiceProvider" --tag=lang
  </p>
  <p class="margin-bottom-0">
    を実行します。
  </p>
  <p class="margin-bottom-0 line-through"><code>resources/lang/vendor/azuki/ja</code>ディレクトリが出力されるので、このディレクトリを
    <code>resources/lang/ja</code>として、コピーあるいは移動します。
  </p>
  <p class="margin-bottom-0">
    Version:3.0.0にて<code>resources/lang/ja</code>に直接展開するように変更しました。<br>
    また、<code>resources/lang/vendor/azuki/ja</code>ディレクトリには名前スペース付きのlangファイルとして
    扱う言語ファイルを展開しています。
  </p>

</div>
