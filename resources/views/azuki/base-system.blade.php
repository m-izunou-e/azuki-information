<h2 id="base-system-azuki">Azukiシステムの基本機能</h2>
<p>
  ここでは、システム構成、パブリッシュ、ログ、設定ファイルについて解説します
</p>
<h3>システム構成</h3>
<div class="section-block">
  <p>
    Azukiシステムは初期状態として３階層の管理システムと一般公開されるサイトで構成されます。<br>

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', '階層名', 'URL', '概要'
    ],
    'body' => [
        [
            'システム管理',
            '/system',
            'システム全般の管理機能を提供する管理画面',
        ],
        [
            '組織管理',
            '/manage',
            'サイトやグループごとの管理機能を提供する管理画面',
        ],
        [
            'ユーザーマイページ',
            '/mypage',
            '各ユーザーに提供される管理画面',
        ],
        [
            'フロント画面',
            '/',
            '一般公開されるサイト',
        ],
    ],
])

  </p>

  <h4>システム管理</h3>
  <div class="section-block">
    <p>
      <code>DirectorsTableSeeder.php</code>で設定したログイン情報にてログインできます。<br>
      システム管理者のログインIDはemailになります。
    </p>
    <p>
      システム管理では以下の管理機能を提供しています。

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', 'メニュー名', 'URL', '概要'
    ],
    'body' => [
        [
            'システム管理者',
            '/system/directors',
            'システム管理者の登録・編集・削除を行います',
        ],
        [
            'サイト管理者',
            '/system//managers',
            '組織管理階層管理の登録・編集・削除を行います
             本システムで組織管理者は一般にサイト管理者として扱っています',
        ],
        [
            'ユーザー管理',
            '/system/users',
            'ユーザーの登録・編集・削除を行います
             ユーザーはフロント画面からもユーザー自身で登録されたものもあります
             また、ユーザー自身により編集も行われます',
        ],
        [
            'ロール管理',
            '/system/system-roles',
            'システム管理者に割り振ることが出来る役割を管理します
             <code>MasterDataTableSeeder.php</code>により、スーパーバイザーとユーザー管理者という二つのロールを初期設定しています
             ロールごとにアクセス制限を設定できます。アクセス制限の登録はURLベースとなりワイルドカードが使用できます
             具体的な判定方法は<code>request->is(\'url\')</code>です',
        ],
        [
            '組織管理',
            '/system/organizations',
            '所属機能が有効場合、ここで組織の登録・編集。削除を行います',
        ],
        [
            'アクセスログ',
            '/system/access-logs',
            'アクセスログ機能が有効の場合、アクセスログを確認できます',
        ],
        [
            'ログインログ',
            '/system/login-logs',
            'ログインログ機能が有効の場合、ログインログを確認できます',
        ],
        [
            'オペレーションログ',
            '/system/operation-logs',
            'オペレーションログ機能が有効の場合、オペレーションログを確認できます',
        ],
        [
            'システムログ',
            '/system/system-logs',
            'システムログを確認できます',
        ],
    ],
])

    </p>
    <p class="annotation">
      本システムはサイト管理者の階層を店舗ごとや組織ごとといったグループ（以下、所属と呼びます）に分けて管理することができます<br>
      そのため組織管理と呼称していますが、所属機能は初期設定では無効でありその場合、サイト管理者としての位置づけとしてとらえています
    </p>

  </div>

  <h4>組織管理</h3>
  <div class="section-block">
    <p>
      システム管理にて登録したサイト管理者の情報でログインできます<br>
      サイト管理者のログインIDはメールアドレスではなくログインIDです<br>
      メールアドレスは任意項目ですが、メールアドレスの登録がないユーザーはパスワードリマインダーが利用できません
    </p>

    <p>
      組織管理では以下の管理機能を提供しています。

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', 'メニュー名', 'URL', '概要'
    ],
    'body' => [
        [
            '管理者',
            '/manage//managers',
            '管理者の登録・編集・削除を行います
             所属が有効の場合、同じ所属のデータのみ操作可能です',
        ],
        [
            'ユーザー',
            '/manage/users',
            'ユーザーの登録・編集・削除を行います
             現状では所属が有効でもユーザーは全所属に共通となり、所属による制限はかかりません',
        ],
        [
            'サンプル',
            '/manage/sample',
            '本システムで構築されたサンプルの管理画面です
             サンプル画面を有効にすることでアクセス可能になります',
        ],
    ],
])

    <p class="annotation">
      所属が有効であってもサイト管理者はmanagersテーブルにて一意のログインID制限でバリデートしており、所属が違っていても登録済みのログインIDは使用できません<br>
      所属有効時にログインIDを所属ごとに一意とする方法は別途記述する予定です。
    </p>

  </div>

  <h4>ユーザーマイページ</h3>
  <div class="section-block">
    <p>
      ユーザー情報の編集ができます。
    </p>
  </div>



  <h3>パブリッシュ</h3>
  <div class="section-block">
    <p>
      seed時記載したartisanのpublishコマンド
      <p class="command">
        $ php artisan vendor:publish --provider="Azuki\ServiceProvider" --tag=seed
      </p>
      にて本システムが提供しているタグとその対象について説明します
    </p>
    <p>
      タグの一覧は以下となります

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', '対象', 'タグ名', '出力先', '概要'
    ],
    'body' => [
        [
            '設定',
            'config',
            'config/',
            '設定ファイルをパブリッシュします。
             パプリッシュされる設定ファイルは以下となります'.
            '<ul class="no-decoration">'.
            '  <li><code>azuki.app.php</code></li>'.
            '  <li><code>azuki.standard.php</code></li>'.
            '  <li><code>azuki.auth.php</code></li>'.
            '  <li><code>azuki.execute_type.php</code></li>'.
            '  <li><code>azuki.master.php</code></li>'.
            '  <li><code>azuki.mail.php</code></li>'.
            '  <li><code>azuki.upload_file.php</code></li>'.
            '  <li><code>azuki.menu_list.php</code></li>'.
//            '  <li><code>azuki.view.php</code></li>'.
            '  <li><code>azuki.icon.php</code></li>'.
            '  <li><code>00_define_common.php</code></li>'.
            '</ul>',
        ],
        [
            '設定',
            'azuki-app',
            'config/',
            'Contratsの実体設定ファイルである<code>azuki.app.php</code>だけをパブリッシュします',
        ],
        [
            '設定',
            'azuki-config',
            'config/',
            '基本設定ファイルである<code>azuki.standard.php</code>だけをパブリッシュします',
        ],
        [
            '定義',
            'define',
            'config/',
            '定義ファイルである<code>00_define_common.php</code>だけをパブリッシュします',
        ],
        [
            'テンプレート',
            'view',
            'resources/views/vendor/azuki/',
            'テンプレートファイルをパブリッシュします
             パブリッシュされたテンプレートファイルを変更することでUIの変更ができます',
        ],
        [
            'シード',
            'seed',
            'database/seeds/',
            'シーダーをパブリッシュします
             シーダーはパブリッシュ後<code>composer dump-autoload</code>することでartisanから使用可能になります',
        ],
        [
            '言語',
            'lang',
            'resources/views/vendor/azuki/',
            '言語ファイルをパブリッシュします
             エラーメッセージの日本語化に必要です',
        ],
        [
            'ルーティング',
            'route',
            'routes/',
            'ルーティングファイル<code>azuki.web.php</code>をパブリッシュします
             出力されたルーティングファイルを修正することでルーティングを変更できます',
        ],
        [
            '公開',
            'public',
            'public/vendor/azuki/',
            '公開ディレクトリへ展開が必要なファイルをパブリッシュします。
             パプリッシュされるディレクトリは以下となります'.
            '<ul class="no-decoration">'.
            '  <li><code>public/vendor/azuki/foundation-icons</code></li>'.
            '  <li><code>public/vendor/azuki/css</code></li>'.
            '  <li><code>public/vendor/azuki/img</code></li>'.
            '  <li><code>public/vendor/azuki/js</code></li>'.
            '</ul>',
        ],
        [
            '未コンパイルJS/CSS',
            'assets',
            'resources/sass/vendor/azuki/ resources/js/vendor/azuki/',
            '本システムはfoundationを使用し、sassを用いて<code>app.css</code><code>app.js</code>を作成しています。
             これらファイルをコンパイルする元ファイルを出力します',
        ],
    ],
])

    </p>
  </div>

  <h3>ログ</h3>
  <div class="section-block">
    <p>

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', 'ログ名', 'テーブル名', '管理画面', '概要'
    ],
    'body' => [
        [
            'メール',
            'azuki_mail_logs',
            '×',
            'システムからのメール送信を記録します
             送信前にテーブルに記録し、送信後にresult.failuresカラムを更新します
             resultが0のものは送信後のデータ更新がされていないレコードになります
             これは送信処理が正常に行われていない可能性を示唆します。
             また、メールサーバーからのエラーメールを補足する仕組みは入っていません
             ですので、メール送信結果はsendmailコマンドやメールサーバーのレスポンスに依存したものであり送信できたことを保証するものではありません',
        ],
        [
            'システム',
            'azuki_system_logs',
            '〇',
            'システムの動作に関するログを記録します
             コマンドで定期的に処理しているタスクなどシステムの動作状況を確認するのに役立つログをここに残すように想定しています',
        ],
        [
            'オペレーション',
            'azuki_operation_logs',
            '〇',
            'データ操作に関するログを記録します
             対象となったデータと変更点、操作した人などを確認できます
             各ログへの記録は対象外としています',
        ],
        [
            'アクセス',
            'azuki_access_logs',
            '〇',
            'サイトへのアクセスを記録します
             アクセスされた日時、URL、アクセスした人、アクセス時の$_SERVERを確認できます',
        ],
        [
            'ログイン',
            'azuki_login_logs',
            '〇',
            'ログイン情報を記録します
             誰がいつログインしたかを確認できます',
        ],
    ],
])

      ログの記録有無はメールログを除き、設定で有効・無効を切り替えることができます<br>
      各ログテーブルは大量のデータになることが想定されますので、適度にメンテナンスしてください
    </p>
  </div>

  <h3>設定ファイル</h3>
  <div class="section-block">
    <p>
      設定ファイルは<code>azuki.auth.php</code>ならびに<code>00_define_common.php</code>を除き、Laravelの仕組みによって
      マージ処理されます<br>
      ※<code>azuki.auth.php</code>は原則として<code>auth.php</code>を上書きします<br>
      各設定ファイルの詳細な設定内容については、各設定ファイルを参照してください

@include($vendor.'azuki.001-parts-table', [
    'number' => true,
    'head' => [
        '', '設定ファイル名', '概要'
    ],
    'body' => [
        [
            'azuki.app.php',
            'AzukiシステムのContractsで登録されているクラスの実体設定を行います
             システムで設定されている実態を置き換える場合にこの設定ファイルで置き換えの指定ができます',
        ],
        [
            'azuki.standard.php',
            'Azukiパッケージの基本設定を管理します
             主要な設定内容は'.
            '<ul class="no-decoration">'.
            '  <li><code>enable_azuki_error_handler</code>[def:true|env:AZUKI_ERROR_HANDLER]</li>'.
            '  <li><code>auth.overwrite</code>[def:true]</li>'.
            '  <li><code>routing</code>[def:true]</li>'.
            '  <li><code>organizations</code>[def:false|env:AZUKI_ORGANIZATION_SUPPORT]</li>'.
            '  <li><code>contents</code>[コンテンツの制御]</li>'.
            '  <li><code>url</code>[URLベースの制御]</li>'.
            '  <li><code>record_access_logs</code>[def:true|env:AZUKI_RECORD_ACCESS_LOGS]</li>'.
            '  <li><code>record_login_logs</code>[def:true|env:AZUKI_RECORD_LOGIN_LOGS]</li>'.
            '  <li><code>record_operation_logs</code>[def:true|env:AZUKI_RECORD_OPERATION_LOGS]</li>'.
            '  <li><code>record_system_logs</code>[def:true|env:AZUKI_RECORD_SYSTEM_LOGS]</li>'.
            '  <li><code>subDir</code>[システム管理（/system）サイト管理（/manage）URLパスの置き換え設定]</li>'.
            '</ul>'.
            '',
        ],
        [
            'azuki.auth.php',
            'Azukiシステムの各管理者ならびにユーザーの認証を行うための設定です
             Laravelのauth.phpの設定を強制的に上書きしてシステムを起動します',
        ],
        [
            'azuki.execute_type.php',
            'まとまった処理実行を行うための仕組みとして用意しているExecuterの設定を行います',
        ],
        [
            'azuki.icon.php',
            'システム内で使用している各種アイコンの設定を行います。ここで指定した内容は<code>&lt;i&gt;</code>タグのclass属性に指定されます
             ※メニューのアイコン設定はazuki.menu_list.phpで行います',
        ],
        [
            'azuki.master.php',
            'マスターデータの定義を行っている設定ファイルです
             マスターデーターが必要な場合、このファイルに記述することでAzukiシステム内で簡単に使用することができます
             データベースで定義しているマスターデータもここに記述します',
        ],
        [
            'azuki.mail.php',
            'メール送信に関する追加設定です
             ここにメール送信に関する設定を追記することでメール送信の実装が楽になります',
        ],
        [
            'azuki.upload_file.php',
            'ファイルのアップロードに関する設定を行います
             実際のアップロードを処理するクラスやサイズ、キャッシュの有無、保存先などを設定できます
             ここで定義したtypeをコントローラ側で設定することでアップロード機能が簡単に実装できるようになっています',
        ],
        [
            'azuki.menu_list.php',
            'システム管理、サイト管理画面のグローバルメニューの定義です
             将来的にDB管理に代わる可能性があります',
        ],
        [
            'azuki.icon.php',
            '各メニューアイコン以外のアイコン設定を行います
             アイコンの指定は&lt;i&gt;タグのclassで指定します
             本システムはfoundation-iconを使用しているのでその場合は「fi-xxxx」形式になります
             メニューのアイコンはazuki.menu_list.phpファイルの方で設定します',
        ],
        [
            '00_define_common.php',
            '各種定義ファイルです
             このファイルで定義したdefineを使うことで直接値を使用することなく、コードの整合性を取るように設計しています',
        ],
    ],
])

      設定ファイルはパブリッシュされた設定ファイルがあればそちらが有効になります。
    </p>
  </div>

</div>
