      <p>
        フォームレイアウトは基本構成は、<code>row</code>、<code>cell</code>、<code>parts</code>
        のテンプレートからなり、以下のような形で構成されます<br>
      </p>
<style>
div.my-row {
    border:1px solid #CCCCCC;
    background-color:#CDFFFF;
    padding-top:2rem;
    position:relative;
    margin-right:0 !important;
    margin-left:0 !important;
}
div.my-row p.my-row {
    position:absolute;
    top:0.2em;
    left:1em;
}

div.my-cell {
    border:1px solid #CCCCCC;
    background-color:#99FFFF;
}
div.my-cell p.my-cell {
    margin-bottom:0;
}

div.my-parts {
    background-color:#00CFCF;
    margin-bottom:1em;
}
div.my-parts p.my-parts {
    margin-bottom:0;
    padding-left:1em;
}

</style>

      <div class="row grid-padding-x my-row">
        <p class="my-row">row-1</p>
        <div class="medium-3 cell my-cell">
          <p class="my-cell">cell-1&nbsp;&nbsp;size:3</p>
          <div class="my-parts">
            <p class="my-parts">parts</p>
          </div>
        </div>
        <div class="medium-8 cell my-cell">
          <p class="my-cell">cell-2&nbsp;&nbsp;size:8</p>
          <div class="my-parts">
            <p class="my-parts">parts</p>
          </div>
        </div>
      </div>

      <div class="row grid-padding-x my-row">
        <p class="my-row">row-2</p>
        <div class="medium-3 cell my-cell">
          <p class="my-cell">cell-1&nbsp;&nbsp;size:3</p>
          <div class="my-parts">
            <p class="my-parts">parts</p>
          </div>
        </div>
        <div class="medium-8 cell my-cell">
          <p class="my-cell">cell-2&nbsp;&nbsp;size:8</p>
          <div class="row grid-padding-x my-row">
            <p class="my-row">row-2-1</p>
            <div class="medium-11 cell my-cell">
              <p class="my-cell">cell-2-1-1&nbsp;&nbsp;size:11</p>
              <div class="my-parts">
                <p class="my-parts">parts</p>
              </div>
            </div>
          </div>
          <div class="row grid-padding-x my-row">
            <p class="my-row">row-2-2</p>
            <div class="medium-5 cell my-cell">
              <p class="my-cell">cell-2-2-1&nbsp;&nbsp;size:5</p>
              <div class="my-parts">
                <p class="my-parts">parts</p>
              </div>
            </div>
          </div>
          <div class="row grid-padding-x my-row">
            <p class="my-row">row-2-3</p>
            <div class="medium-8 cell my-cell">
              <p class="my-cell">cell-2-3-1&nbsp;&nbsp;size:8</p>
              <div class="my-parts">
                <p class="my-parts">parts</p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <p class="margin-top-1">
        <code>parts</code>の部分に<code>input,select,textarea</code>といったフォームタグが展開されます<br>
        説明のための余白やマージンを設定しているので、実際とは少し違う形になります
      </p>
