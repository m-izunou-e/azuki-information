<h3 id="install-azuki">Azukiパッケージをインストールする</h3>
<div class="section-block">
  <p>
    laravelをインストールしたディレクトリで<br>
    <p class="command">
      $ composer require la-cuppe/azuki
    </p>
    を実行してください。
    <p class="annotation">
      開発バージョンを取得するには<code>dev-master</code>を引数の最後につけてください
    </p>
  </p>
</div>
