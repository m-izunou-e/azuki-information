<h2 id="about-install">Azukiシステムのインストール</h2>
<div class="section-block">
  <p>インストール手順は
    <ul data-smooth-scroll>
      <li><a href="#install-laravel">Laravelをインストールする</a></li>
      <li><a href="#install-azuki">Azukiパッケージをインストールする</a></li>
      <li><a href="#init-azuki">Azukiパッケージの初期設定を行う</a></li>
    </ul>
    の３ステップ。<br>
    その後、<a href="{{$story}}azuki/develop">{{$pageList['develop']['title']}}</a>を行ってください。
  </p>
</div>
