<h3 id="install-laravel">Laravelをインストールする</h3>
<div class="section-block">
  <p>
    <a href="https://readouble.com/laravel/6.x/ja/installation.html" target='_blank'>Laravel 6.x インストール</a><br>
    <a href="https://readouble.com/laravel/9.x/ja/installation.html" target='_blank'>Laravel 9.x インストール</a>
    &nbsp;(別タブで開きます)&nbsp;
    を参考にLaravelをインストールしてください。<br>
    バージョン指定なしでインストールを行うとカレントバージョンがインストールされます。<br>
    インストールする際にはcomposerで明示的にバージョン指定を行う方が確実です。
  </p>
  <p class="command">
    $ composer create-project --prefer-dist laravel/laravel [ディレクトリ名] [Laravelのバージョン：6.xや9.x]
  </p>
  <p class="annotation">
    LaravelバージョンとAzukiバージョンの対応は<a href="{{$story}}azuki/about">{{$pageList['about']['title']}}</a>をご覧ください。
  </p>
  <p class="line-through">
    Laravel7.x以上で使用する場合は
  </p>
  <p class="command margin-bottom-0 line-through">
    $ composer require laravel/ui
  </p>
  <p class="margin-bottom-0 line-through">
      を実行してください。認証スカフォールドが&nbsp;laravel/ui&nbsp;に移動されたためです。
  </p>
  <p class="annotation">
      Azukiのcomposer.jsonにてrequire指定しているためAzuki4系以降ではAzukiのインストールでインストールされます。
  </p>
  <p>
    Laravelインストール後、<code>アプリケーションキーの設定</code>は忘れずに行ってください。
  </p>

  <p class="margin-bottom-0">
    publicディレクトリを変更したい場合は、<code>[app\Providers\AppServiceProvider.php]</code>の&nbsp;register&nbsp;メソッドにて<br>
  </p>
    <pre><code class="prettyprint linenums block">
    public function register()
    {
        $publicPath = base_path() . '/html';
        // Laravel9.x系は
        $this->app->instance('path.public', $publicPath);
        // Laravel10.x以降は
        $this->app->usePublicPath($publicPath);
    }
    </code></pre>

  <p class="margin-bottom-0">
    （$publicPathは設定したいpublicディレクトリのフルパスです）
    のように設定して変更することが可能です。
  </p>
  <p class="annotation">
    Laravel10.xでは新たにusePublicPathメソッドが追加され、Lravel自体がpublicパスの変更に対応しています。
    <a href="https://readouble.com/laravel/10.x/ja/upgrade.html#public-path-binding" target="_blank">Laravel10.x Publicパスの結合</a>
  </p>

  <p>
    ※Laravel初期ミドルウェアに関する注意<br>
    Laravelでは<code>[app\Http\Kernel.php]</code>にて
    <code>[\Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class]</code>というミドルウェアが登録されています<br>
    これは、ポスト値が空の場合にNULLに変更するミドルウェアです。
    ただし、空文字をポストするとデータを空で登録したい場合に都合が悪くなります。そのような挙動が必要な場合はこのミドルウェアを無効に
    する必要があります。<br>
    Azukiシステムはこのミドルウェアを無効にしていることを前提としています<br>
    無効にするには<code>[\Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class]</code>をコメントアウトします。
  </p>

</div>
{{--

--}}
