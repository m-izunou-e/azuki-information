<h2 id="about-azuki">Azukiシステムとは</h2>
<div class="section-block">
  <p>Laravelをベースとした管理システムを構築するためのパッケージです</p>
  <p>対応バージョンは以下のようになります。

@include($vendor.'azuki.001-parts-table', [
    'number' => false,
    'head' => [
        'Laravel', 'Azuki', '対応', '備考'
    ],
    'body' => [
        [
            '6LTS',
            '>=1.0.0,<4.0.0',
            '〇',
            '6LTSのための最終メジャーバージョンは3。'
        ],
        [
            '7,8',
            '>=2.0.0,<4.0.0',
            '△',
            '7,8での使用は自前で修正が出来る人が対象として、一応可能程度。'
        ],
        [
            '9LTS',
            '>=4.0.0',
            '〇',
            '大きな違いはSwiftMail->SymfonyMailへの対応'
        ],
        [
            '9LTS(>=9.35)',
            '>=4.2.0',
            '〇',
            "Controllerミドルウェア呼び出しロジックの変更に対応。\n4.49での一部バリデーションメソッド引数変更に対応"
        ],
        [
            '10.x',
            '>=5.0.0',
            '〇',
            "composer.jsonの変更とテスト系ファイルの変更"
        ],
        [
            '11.x',
            '>=6.0.0',
            '〇',
            "composer.jsonの変更。\nappディレクトリ以下のProvidersやExceptionクラスの削除に合わせ継承元を変更"
        ],
    ],
])
  </p>
  <p>
    インストール後の続きは「<a href="{{$story}}azuki/system">{{$pageList['system']['title']}}</a>」あるいは
    「<a href="{{$story}}azuki/develop">{{$pageList['develop']['title']}}</a>」をご覧ください。
  </p>
  <p class="annotation">
    本ページは設定により非表示化できます
  </p>
</div>
