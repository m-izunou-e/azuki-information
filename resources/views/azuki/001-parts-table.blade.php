<table>
  <thead>
    <tr>
      @foreach($head as $th)
      <th>{{$th}}</th>
      @endforeach
    </tr>
  </thead>
  <tbody>
    @foreach($body as $line => $row)
    <tr>
      @if(isset($number) && $number === true)
      <td>{{ $line+1 }}.</td>
      @endif
      @foreach($row as $td)
      <td>{!! nl2br($td) !!}</td>
      @endforeach
    </tr>
    @endforeach
  </tbody>
</table>

