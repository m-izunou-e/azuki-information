@extends($vendorPrefix.'layouts.common_simple')

@section('headMeta')
@parent
<meta name="description" content="la-cuppe/azukiとはLaravelをベースにしたWebシステム開発サポートベースシステムです" />
<meta name="keywords" content="Laravel laravel azuki Azuki framwork フレームワーク 開発 ベースシステム" />
@stop

@section('headLink')
@parent
@stop

@section('headCss')
<script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js"></script>
@parent
<link rel="stylesheet" href="/vendor/azuki/css/atelier-lakeside-light.min.css">
<link rel="stylesheet" href="/vendor/azuki/css/azuki.css">
@stop

@section('headScript')
@parent
<script src="/vendor/azuki/js/azuki.js"></script>
@stop

@section('title')
<title>Azukiシステム</title>
@stop

@section('contents-body')
  <div class="row article">
    <div class="large-12 medium-12 small-12">

@yield('pre-section')

@foreach($pageParts as $parts)
  @include($vendor.'azuki.'.$parts)
@endforeach

@yield('post-section')

    </div>
  </div>

  <div class="to-page-top">
    <a href="#head-line" data-smooth-scroll><i class="{{$arrowUpIcon}}"></i></a>
  </div>
  <div class="toc">
    <button id="toc-button" class="button primary">目次</button>
  </div>
@stop

@section('modalContents')
  <div id="azuki-table-of-contents">
    <ul>
    @foreach($pageList as $url => $settings)
      <li><a href="{{$story}}azuki/{{$url}}">{{$settings['title']}}</a>
      @if(isset($settings['link']))
        <ul>
        @foreach($settings['link'] as $link)
          @if($pageKind == $url)
          <li><a href="#{{$link['anchor']}}" data-smooth-scroll>{{$link['title']}}</a></li>
          @else
          <li><a href="{{$story}}azuki/{{$url}}#{{$link['anchor']}}">{{$link['title']}}</a></li>
          @endif
        @endforeach
        </ul>
      @endif
      </li>
    @endforeach
    </ul>
    <button id="close-toc" class="button primary">閉じる</button>
  </div>
@stop
