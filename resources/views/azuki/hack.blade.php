<h2 id="hack">Azukiシステムをハックする</h2>
<div class="section-block">
  <p>
    Azukiで出来る基本的な内容は以下の項目でまとめています<br>
    <ul>
      <li><a href="#hack-0">実体クラスの差し替えについて</a></li>
      <li><a href="#hack-1">フォームエレメントの設定</a></li>
      <li><a href="#hack-2">CtrlSupporterの設定</a></li>
      <li><a href="#hack-3">バリデーションについて</a></li>
      <li><a href="#hack-4">モデルについて</a></li>
      <li><a href="#hack-5">メール送信について</a></li>
    </ul>

    Azuki自体に手を入れたい場合にどうすればいいかを説明する<br>
    <ul>
      <li><a href="#hack-10">ハックする</a></li>
    </ul>
  </p>

  <h3 id="hack-0">実体クラスの差し替えについて</h3>
  <div class="section-block">
    <p>
      機能の拡張や変更をする際に実装を最小限とするため、
      既存のクラスを継承した新しいクラスを作成し必要な実装のみ行うことを推奨しています。<br>
      Azukiシステムでは、システム内で使用しているクラスの実体を簡単に差し替え出来るようにしています<br>
      実体の差し替えは<code>azuki.app.php</code>ファイルで定義できます
    </p>
    <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '初期クラス', '備考'
    ],
    'body' => [
        [
            '--コントローラ--',
            '',
            '',
        ],
        [
            'controllers.system.index',
            '\Azuki\App\Http\Controllers\System\IndexController',
            'システム管理ダッシュボード画面',
        ],
        [
            'controllers.system.login',
            '\Azuki\App\Http\Controllers\System\LoginController',
            'システム管理ログイン画面',
        ],
        [
            'controllers.system.profile',
            '\Azuki\App\Http\Controllers\System\ProfileController',
            'システム管理プロフィール画面',
        ],
        [
            'controllers.system.directors',
            '\Azuki\App\Http\Controllers\System\DirectorsController',
            'システム管理者管理画面',
        ],
        [
            'controllers.system.managers',
            '\Azuki\App\Http\Controllers\System\ManagersController',
            'システム管理サイト管理者管理画面',
        ],
        [
            'controllers.system.users',
            '\Azuki\App\Http\Controllers\System\UsersController',
            'システム管理ユーザー管理画面',
        ],
        [
            'controllers.system.systemroles',
            '\Azuki\App\Http\Controllers\System\SystemRolesController',
            'システム管理ロール管理画面',
        ],
        [
            'controllers.system.organizations',
            '\Azuki\App\Http\Controllers\System\OrganizationsController',
            'システム管理組織管理画面',
        ],
        [
            'controllers.system.accesslogs',
            '\Azuki\App\Http\Controllers\System\AccessLogsController',
            'システム管理アクセスログ画面',
        ],
        [
            'controllers.system.loginlogs',
            '\Azuki\App\Http\Controllers\System\LoginLogsController',
            'システム管理ログインログ画面',
        ],
        [
            'controllers.system.operationlogs',
            '\Azuki\App\Http\Controllers\System\OperationLogsController',
            'システム管理操作ログ画面',
        ],
        [
            'controllers.system.systemlogs',
            '\Azuki\App\Http\Controllers\System\SystemLogsController',
            'システム管理システムログ画面',
        ],
        [
            'controllers.system.eqs',
            '\Azuki\App\Http\Controllers\System\ExecuteQueueStatusController',
            'システム管理コマンド実行状況画面',
        ],
        [
            'controllers.system.resetpassword',
            '\Azuki\App\Http\Controllers\System\ResetPasswordController',
            'システム管理パスワードリセット機能',
        ],
        [
            'controllers.system.fileupload',
            '\Azuki\App\Http\Controllers\System\FileUploadController',
            'システム管理ファイルアップロード機能',
        ],
        [
            '',
            '',
            '',
        ],
        [
            'controllers.manage.index',
            '\Azuki\App\Http\Controllers\Manage\IndexController',
            'サイト管理ダッシュボード',
        ],
        [
            'controllers.manage.login',
            '\Azuki\App\Http\Controllers\Manage\LoginController',
            'サイト管理ログイン画面',
        ],
        [
            'controllers.manage.profile',
            '\Azuki\App\Http\Controllers\Manage\ProfileController',
            'サイト管理プロフィール画面',
        ],
        [
            'controllers.manage.managers',
            '\Azuki\App\Http\Controllers\Manage\ManagersController',
            'サイト管理サイト管理者管理画面',
        ],
        [
            'controllers.manage.users',
            '\Azuki\App\Http\Controllers\Manage\UsersController',
            'サイト管理ユーザー管理画面',
        ],
        [
            'controllers.manage.sample',
            '\Azuki\App\Http\Controllers\Manage\SampleController',
            'サイト管理サンプル画面',
        ],
        [
            'controllers.manage.eqs',
            '\Azuki\App\Http\Controllers\Manage\ExecuteQueueStatusController',
            'サイト管理コマンド実行状況画面',
        ],
        [
            'controllers.manage.resetpassword',
            '\Azuki\App\Http\Controllers\Manage\ResetPasswordController',
            'サイト管理パスワードリセット機能',
        ],
        [
            'controllers.manage.fileupload',
            '\Azuki\App\Http\Controllers\Manage\FileUploadController',
            'サイト管理ファイルアップロード機能',
        ],
        [
            '',
            '',
            '',
        ],
        [
            'controllers.common.index',
            '\Azuki\App\Http\Controllers\Common\IndexController',
            'フロントトップ画面',
        ],
        [
            'controllers.common.login',
            '\Azuki\App\Http\Controllers\Common\LoginController',
            'ユーザーログイン画面',
        ],
        [
            'controllers.common.mypage',
            '\Azuki\App\Http\Controllers\Common\MypageController',
            'マイページ',
        ],
        [
            'controllers.common.regist',
            '\Azuki\App\Http\Controllers\Common\RegistController',
            'ユーザー登録画面',
        ],
        [
            'controllers.common.resetpassword',
            '\Azuki\App\Http\Controllers\Common\ResetPasswordController',
            'ユーザーパスワードリセット機能',
        ],
        [
            'controllers.common.uploadfileview',
            '\Azuki\App\Http\Controllers\Common\UploadedFileViewController',
            'アップロードファイル閲覧機能',
        ],
        [
            '--モデル--',
            '',
            '',
        ],
        [
            'models.directors',
            '\Azuki\App\Models\Directors',
            'システム管理者モデル',
        ],
        [
            'models.managers',
            '\Azuki\App\Models\Managers',
            'サイト管理者モデル',
        ],
        [
            'models.users',
            '\Azuki\App\Models\Users',
            'ユーザーモデル',
        ],
        [
            'models.systemroles',
            '\Azuki\App\Models\SystemRoles',
            'ロール管理モデル',
        ],
        [
            'models.organizations',
            '\Azuki\App\Models\Organizations',
            '組織モデル',
        ],
        [
            'models.accesslogs',
            '\Azuki\App\Models\AccessLogs',
            'アクセスログモデル',
        ],
        [
            'models.loginlogs',
            '\Azuki\App\Models\LoginLogs',
            'ログインログモデル',
        ],
        [
            'models.operationlogs',
            '\Azuki\App\Models\OperationLogs',
            '操作ログモデル',
        ],
        [
            'models.systemlogs',
            '\Azuki\App\Models\SystemLogs',
            'システムログモデル',
        ],
        [
            'models.eqs',
            '\Azuki\App\Models\ExecuteTargetQueue',
            'コマンド実行キューモデル',
        ],
        [
            'models.sample',
            '\Azuki\App\Models\Sample',
            'サンプルモデル',
        ],
        [
            '--バリデータ--',
            '',
            '',
        ],
        [
            'validators.system.profile',
            '\Azuki\App\Http\Validation\System\ProfileValidator',
            'システム管理プロフィールバリデータ',
        ],
        [
            'validators.system.directors',
            '\Azuki\App\Http\Validation\System\DirectorsValidator',
            'システム管理システム管理者バリデータ',
        ],
        [
            'validators.system.managers',
            '\Azuki\App\Http\Validation\System\ManagersValidator',
            'システム管理サイト管理者バリデータ',
        ],
        [
            'validators.system.users',
            '\Azuki\App\Http\Validation\System\UsersValidator',
            'システム管理ユーザーバリデータ',
        ],
        [
            'validators.system.systemroles',
            '\Azuki\App\Http\Validation\System\SystemRolesValidator',
            'システム管理ロール管理バリデータ',
        ],
        [
            'validators.system.organizations',
            '\Azuki\App\Http\Validation\System\OrganizationsValidator',
            'システム管理組織管理バリデータ
             共通バリデータである\Azuki\App\Http\Validation\SharedValidator::classを使っています',
        ],
/*
        [
            'validators.system.accesslogs',
            '\Azuki\App\Http\Validation\System\AccessLogsValidator',
            'システム管理アクセスログ管理バリデータ
             共通バリデータである\Azuki\App\Http\Validation\SharedValidator::classを使っています',
        ],
        [
            'validators.system.loginlogs',
            '\Azuki\App\Http\Validation\System\LoginLogsValidator',
            '',
        ],
        [
            'validators.system.operationlogs',
            '\Azuki\App\Http\Validation\System\OperationLogsValidator',
            '',
        ],
        [
            'validators.system.systemlogs',
            '\Azuki\App\Http\Validation\System\SystemLogsValidator',
            '',
        ],
        [
            'validators.system.eqs',
            '\Azuki\App\Http\Validation\System\ExecuteQueueStatusValidator',
            '',
        ],
*/
        [
            '',
            '',
            '',
        ],
        [
            'validators.manage.managers',
            '\Azuki\App\Http\Validation\Manage\ManagersValidator',
            'サイト管理サイト管理者バリデータ',
        ],
        [
            'validators.manage.users',
            '\Azuki\App\Http\Validation\Manage\UsersValidator',
            'サイト管理ユーザーバリデータ',
        ],
        [
            'validators.manage.sample',
            '\Azuki\App\Http\Validation\Manage\SampleValidator',
            'サイト管理サンプルバリデータ',
        ],
/*
        [
            'validators.manage.eqs',
            '\Azuki\App\Http\Validation\Manage\ExecuteQueueStatusValidator',
            '',
        ],
*/
        [
            '',
            '',
            '',
        ],
        [
            'validators.common.mypage',
            '\Azuki\App\Http\Validation\Common\UsersValidator',
            'ユーザーバリデータ',
        ],
    ],
])
    </p>
    <p>
      差し替えるクラスはそれぞれ初期クラスを継承し作成してください。
    </p>
  </div>


  <h3 id="hack-1">フォームエレメントの設定</h3>
  <div class="section-block">
    <p class="margin-bottom-0">
      フォームエレメントは各コントローラで<code>$elements</code>として定義するメンバ変数で、画面のフォーム要素を構成する設定です<br>
      大きく、レイアウト・フォーム項目の詳細・一覧表示の詳細を設定できます
    </p>
    <p class="annotation margin-bottom-0">
      なお、各画面ごとにどの要素をどの並び順で表示するかの設定は、<code>$elementsOrder</code>メンバ変数で行います。
    </p>
    <pre><code class="prettyprint linenums block">
$elements = [
    // 構成内容としては
    '項目キー（一意なキー名）' => [
        'layout' => [
            レイアウト設定内容
        ],
        'searchLayout' => [
            検索フィールドでのレイアウト設定内容
        ],
        'form' => [
            各フォームの詳細設定
        ],
        'list' => [
            一覧表示の詳細設定
        ],
    ],
    // 具体例としては
    'name' => [
        'form'   => [
            'title' => [
                'size'     => 3,
                'type'     => 'title',
                'name'     => 'お名前',
                'required' => IS_REQUIRED,
            ],
            'name'    => [
                'required'          => IS_REQUIRED,
                'size'              => 8,
                'type'              => 'text',
                'name'              => 'name', 
                'column'            => 'name',
                'placeholder'       => '大阪　太郎',
                'searchName'        => 'search[name]',
                'searchPlaceholder' => '部分一致します',
                'validate'          => [
                    'rule'     => 'required|max:36',
                    'rules'     => [
                        ['type' => 'required'],
                        ['type' => 'max', 'condition' => '36'],
                    ],
                ],
            ],
        ],
        'list' => [
            'name' => [
                'type'           => CONTROL_TYPE_TEXT,
                'width'          => '15%',
                'orderable'      => false,
            ],
        ],
    ],
];
    </code></pre>
    <p>
      <code>項目キー</code>に対して画面構成上の１項目分を設定する構成になります。<br>
      １項目に複数のフォームがある場合、form設定部分に複数のフォーム設定を記述することになります。<br>
      その際にどのような表示レイアウトにするかを決めるのがレイアウト設定になります。
    </p>

    <h4 class="margin-top-1">レイアウト</h4>
    <div class="section-block">
      <p class="margin-bottom-0">
        <code>searchLayout</code>というキーは検索フィールド用のレイアウト指定で、フォーム画面での表示レイアウトと検索フィールドでの
        表示レイアウトが違う場合に指定します。設定方法は<code>layout</code>キーでの指定方法と同様です。<br>
        <code>layout</code>キーの設定を省略した場合、レイアウト設定は<code>['title_項目キー', '項目キー']</code>
        となります。これは、単純な
      </p>
      <pre><code class="prettyprint linenums block">
項目名      ｜入力フォーム
      </code></pre>

      <p>
        のレイアウトになります。
      </p>
      <p class="annotation">
        レイアウト設定で指定された値は<code>form</code>キーで設定されている必要があります。<br>
        <code>'title_項目キー'</code>には<code>form->title</code>が紐づけられます。layout設定をしない限りは
        <code>項目キー->form->title</code>と<code>項目キー->form->項目キー</code>は必須となります。
      </p>

      <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '内容（設定方法）'
    ],
    'body' => [
        [
            'key',
            '項目に対するlayout設定のキー名
             このキー名は項目自体の表示・非表示といったコントロールに使用します
             キー名を設定しなければ常に表示となります。キー名が指定されている場合、<code>isViewRow</code>メソッドにて表示・非表示の判定が行われます
             判定方法は<code>preg_match(\'/.*ページ種類*/u\', キー名)</code>となり、ページ種類は[\'list\', \'form\', \'confirm\']です
             キー名指定した場合、原則非表示となり表示したいページ種類をキー名に含めることでそのページのみ表示されることになります',
        ],
        [
            'val',
            'レイアウト方法の指定です
             基本形としては、指定した要素が横並びになります。サイズは各要素側で指定します
             合計12を超えると１行に収まらないので要素単位で自動的に次行に落ちます
             配列で指定すると配列で指定している要素が縦並びになります
             また、キー名を指定することで、縦並びになる部分のサイズ指定やGroupInputの指定ができます',
        ],
    ],
])
      </p>

      <p class="margin-bottom-0">
        複雑なレイアウトの例として以下のような指定をすると住所の項目をレイアウトすることができます。
      </p>
      <pre><code class="prettyprint linenums block">
'password_conf' => [
    'layout' => [
        'key' => 'view_form',  // 入力・編集画面のみ表示されるようになります
        'val' => [ 'title_password_conf', 'password_conf' ],
    ],
],

'address' => [
    'layout' => [
        'val' => [
            'title_address', 
            'row1_size_9' => [   // _size_9　の部分により、cellのサイズ９が指定されます
                // GroupInputキー名により指定された配列がグループ化されて１行の要素になります
                // GroupInputが複数必要な場合は、キー名にGroupInput1　GroupInput2　のように指定することが可能です
                'GroupInput' => [ 'zipcode1', 'haifun', 'zipcode2', 'searchBtn'],
                'pref',
                'address1',
                'address2',
            ]
        ],
    ],
    'searchLayout' => [
        'val' => [
            'title_address',
            'row1_size_9' => [
                'GroupInput' => [ 'zipcode_label', 'zipcode'],
                'pref',
                'address1',
                'address2',
            ]
        ],
    ],
],
      </code></pre>

      <p>
        address項目は上記の指定で以下のような表示になります<br>
        入力フィールドの場合<br>
        <img class="snap" src="/vendor/azuki/img/azuki/hack-form-field1.png" alt="住所入力フィールド"><br>
        検索フィールドの場合<br>
        <img class="snap" src="/vendor/azuki/img/azuki/hack-search-field1.png" alt="住所検索フィールド"><br>
      </p>
      
{{-- フォームレイアウト構成について --}}
@include($vendor.'azuki.hack-layout')

    </div>

    <h4 class="margin-top-1">フォーム</h4>
    <div class="section-block">
      <p>
        <code>titleキー</code>と<code>要素キー</code>配列で構成します<br>
        <code>項目キー->layout</code>の指定をしなかった場合、項目キーと同じ名前の要素キーの設定が必須になります<br>
      </p>

      <h5>title</h5>
      <div class="section-block">
        <p>
          titleに設定できる項目は以下になります
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '内容（設定方法）'
    ],
    'body' => [
        [
            'name',
            '必須',
            '項目に対する名前の設定です
             詳細・入力・確認画面において項目名として利用される他、一覧表示のTHで利用される名称、
             共通バリデーション機能利用時のattribute値として使用されます',
        ],
        [
            'size',
            '任意',
            '表示サイズの指定です
             省略した場合のデフォルト値は<code>3</code>です。CSSのクラスとして<code>midium-設定値</code>を付けてサイズをコントロールしています',
        ],
        [
            'type',
            '任意',
            '省略した場合<code>title</code>になります
             タイトル要素は原則としてtypeは<code>title</code>固定で問題ありません
             他typeを使用して利用することも可能ですがその場合の説明は割愛します',
        ],
        [
            'required',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             <code>IS_REQUIRED</code>を指定すると、入力画面に[必須]マークが表示されます
             <code>NOT_REQUIRED</code>を指定すると、入力画面に[任意]マークが表示されます
             省略時はどちらのマークもつきません',
        ],
        [
            'required_if_regist',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             新規登録時のみ、requiredの値をここで設定した値に置き換えることになります
             新規登録時のみ必須の場合、requiredに<code>NOT_REQUIRED</code>を指定し、
             ここに<code>IS_REQUIRED</code>を指定します',
        ],
        [
            'required_if_edit',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             編集時のみ、requiredの値をここで設定した値に置き換えることになります
             編集時のみ必須の場合、requiredに<code>NOT_REQUIRED</code>を指定し、
             ここに<code>IS_REQUIRED</code>を指定します',
        ],
    ],
])
        </p>
      </div>

      <h5>要素キー</h5>
      <div class="section-block">
        <p>
          要素キー名は任意です。ただし一意である必要があります。１つの項目設定に対して複数の要素キーを設定できます<br>
          要素キー名はlayoutで指定されることで画面上に表示される対象となります
          要素キーに設定できる項目は以下になります
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '内容（設定方法）'
    ],
    'body' => [
        [
            'name',
            'typeにより
             必須',
            'フォームのname属性として使用します
             typeが<code>label</code>あるいは、<code>button</code>以外の場合は必須です',
        ],
        [
            'label',
            'typeにより
             必須',
            '表示名として使用します
             typeが<code>label</code>あるいは、<code>button</code>の場合は必須です',
        ],
        [
            'size',
            '任意',
            '表示サイズの指定です
             省略した場合のデフォルト値は<code>8</code>です。CSSのクラスとして<code>midium-設定値</code>を付けてサイズをコントロールしています',
        ],
        [
            'type',
            '必須',
            '要素のタイプを文字列で指定します<br><br>'.
            '<ul class="no-decoration">'.
            '  <li><code>text</code>&nbsp;:&nbsp;input type=text を構成します</li>'.
            '  <li><code>textarea</code>&nbsp;:&nbsp;textarea を構成します</li>'.
            '  <li><code>select</code>&nbsp;:&nbsp;select を構成します</li>'.
            '  <li><code>radio</code>&nbsp;:&nbsp;input type=radio を構成します</li>'.
            '  <li><code>checkbox</code>&nbsp;:&nbsp;input type=checkbox を構成します</li>'.
            '  <li><code>datetime</code>&nbsp;:&nbsp;input type=text class=datetime-picker を構成します</li>'.
            '  <li><code>image-upload</code>&nbsp;:&nbsp;画像アップロード用のフィールドを構成します</li>'.
            '  <li><code>movie-upload</code>&nbsp;:&nbsp;動画アップロード用のフィールドを構成します</li>'.
            '  <li><code>label</code>&nbsp;:&nbsp;label を構成します</li>'.
            '  <li><code>button</code>&nbsp;:&nbsp;button を構成します</li>'.
            '</ul>'.
            'typeに対応するテンプレートファイルが存在しないものは全て<code>input type="タイプ名"</code>として処理されます
             (<code>text</code>のテンプレートファイルを使用)
             <code>/resources/vendor/azuki/layouts/parts/share/layout-parts-form/field-タイプ名.blade.php</code>
             としてテンプレートファイルを作成することでタイプを増やす・テンプレートを変更することが可能です
             
             汎用ではありませんが、本システムでは上記以外に<code>serialize-array</code><code>system-role-authorities</code>を使っています
             また、<code>file-upload</code><code>wisywig</code>を作成中です',
        ],
        [
            'column',
            'typeにより
             必須',
            '紐づくDBテーブルのカラム名を指定します
             validateのキー名もnameの値ではなくこの値を使っています。
             存在しないカラム名を指定してもModelクラスのfillで排除されるため原則問題ありません
             登録・編集画面で利用するフォーム要素（label。buttonを除く）については必須項目になります
             登録はしないが、条件判定などに使用したい要素がある場合、存在しないカラム名で要素定義を行うことで可能になります',
        ],
        [
            'select',
            'typeにより
             必須',
            '<code>select</code><code>radio</code><code>checkbox</code>の場合必須です
             ここで指定された文字列に対応するマスターデータを使って自動的に選択肢を構成します
             共通バリデーションを使う場合、validateに自動的に<code>in</code>条件が付加されます',
        ],
        [
            'required',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             <code>IS_REQUIRED</code>の場合、フォーム要素のタグに<code>required aria-required="true"</code>属性が付加されます
             typeが<code>text</code><code>textarea</code><code>select</code><code>datetime</code>で有効です',
        ],
        [
            'required_if_regist',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             新規登録時のみ、requiredの値をここで設定した値に置き換えることになります
             新規登録時のみ必須の場合、requiredを未設定にするか<code>NOT_REQUIRED</code>を指定し、
             ここに<code>IS_REQUIRED</code>を指定します',
        ],
        [
            'required_if_edit',
            '任意',
            '<code>IS_REQUIRED</code>あるいは<code>NOT_REQUIRED</code>を指定します
             編集時のみ、requiredの値をここで設定した値に置き換えることになります
             編集時のみ必須の場合、requiredを未設定にするか<code>NOT_REQUIRED</code>を指定し、
             ここに<code>IS_REQUIRED</code>を指定します',
        ],
        [
            'default',
            '任意',
            '新規登録時のデフォルト値を設定します
             フォームの入力値として初期設定値が必要な場合に使用します
             初期設定「無効」にしたいradioボタンなど',
        ],
        [
            'defString',
            '任意',
            '未設定の場合に画面上に表示したい文字列を指定範囲をキーとした連想配列で設定します
             例）パスワードを詳細画面では[********]として、変更がない場合の確認画面では[変更なし]と表示したい場合
             "detail" => "********",
             "confirm" => "変更なし"
             と設定することで実現できます。指定範囲は["form"、"detail"、"confirm"]を指定できます',
        ],
        [
            'placeholder',
            '任意',
            'プレースフォルダーに表示したい内容を設定します',
        ],
        [
            'searchType',
            '任意',
            '検索フィールド構成時にタイプが変わる場合、ここに記述します
             type：radioの場合、検索フィールドでは複数選択したいことが多くその場合、この値として<code>checkbox</code>とすることで
             入力・編集時はラジオボタン、検索フィールドはチェックボックスにすることができます',
        ],
        [
            'searchName',
            '任意',
            '検索フィールドのname属性を上書きしたい場合に指定します
             検索フィールドのname属性はsearch[xxxx]という形式の名前にする必要があります
             typeが<code>title</code><code>label</code><code>button</code>以外の場合、検索フィールド構成時は自動的に上記の形に置き換えます
             name値がemailだった場合、<code>search[email]</code>に変換します',
        ],
        [
            'searchPlaceholder',
            '任意',
            '検索フィールド時にplaceholder値を上書きする必要がある場合、ここで指定します',
        ],
        [
            'searchXxxxxx',
            '任意',
            '検索フィールド時のみ設定したい設定値、上書き設定したい設定値は上述の<code>searchType</code><code>searchName</code>などのように
             <code>search設定名</code>という形の設定名で指定することができます。',
        ],
        [
            'helperText',
            '任意',
            '要素の下部に表示したい補足説明を設定します
             「※8文字以上16文字以内の英数大文字小文字記号のみ」など',
        ],
        [
            'event',
            '任意',
            'typeが<code>button</code>の場合に指定できます
             ボタン押下時になどボタン操作に対してイベントを登録したい場合に使用します
             例）\'onClick\' => "AjaxZip3.zip2addr(\'zipcode1\', \'zipcode2\', \'pref\', \'address1\', \'address2\')"
             にてクリック時にAjaxZipをコールすることができます',
        ],
        [
            'ufType',
            'typeにより
             必須',
            'アップロードのタイプを指定します
             typeが<code>image-upload</code>や<code>movie-upload</code>などのアップロード系の処理の場合に指定が必要です
             <code>config/azuki.upload_file</code>にて設定しているタイプから該当するタイプを文字列で指定します',
        ],
        [
            'isMulti',
            '任意',
            '複数ファイルの同時アップロードの可否を[trueまたはfalse]で指定します。
             typeが<code>image-upload</code>や<code>movie-upload</code>などのアップロード系の処理の場合に指定できます
             trueの場合、選択ボタンがmultipleになり、複数選択が可能になります',
        ],
        [
            'validate',
            '任意',
            '共通バリデーターを利用する際の設定を行います
             詳細な設定方法は後述します',
        ],
    ],
])
        </p>
      </div>

      <h5>要素キー：validate</h5>
      <div class="section-block">
        <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '内容（設定方法）'
    ],
    'body' => [
        [
            'attribute',
            '任意',
            '属性値の設定が必要な場合に配列で設定します
             未設定の場合、<code>form->title->name</code>が自動的に設定されます
             また、ここで設定する配列のキー名は<code>form->要素キー->column</code>が自動的に補完されます
             例）要素キーが\'address\'の時、\'attribute\' => [\'*.zip\' => \'郵便番号\', \'*.pref\' => \'都道府県\']
                 と設定されていた場合、\'attribute\' => [ \'address.*.zip\' => \'郵便番号\', \'address.*.pref\' => \'都道府県\' ]
                 と設定されます
             validatorクラス側で設定されているものについては上書きされません',
        ],
        [
            'message',
            '任意',
            'エラーメッセージの設定が必要な場合に配列で設定します
             また、ここで設定する配列のキー名は<code>form->要素キー->column</code>が自動的に補完されます
             例）要素キーが\'address\'の時、\'message\' => [\'*.zip.reg\' => \'郵便番号の形式が不正です\']
                 と設定されていた場合、\'message\' => [ \'address.*.zip.reg\' => \'郵便番号の形式が不正です\' ]
                 と設定されます
             validatorクラス側で設定されているものについては上書きされません',
        ],
        [
            'rules',
            '任意',
            '配列で指定します。指定した配列分だけ
             <code>\'バリデーションキー名\' => [rule:condition]</code>としてバリデーションルールが構成されます
             <code>\'バリデーションキー名\'</code>は原則として、<code>form->要素キー->column</code>が設定されます
             指定できる項目は以下となります'.
            '<ul class="no-decoration">'.
            '  <li><code>type</code>&nbsp;:&nbsp;[必須]バリデーションタイプ(ルール)です</li>'.
            '  <li><code>ruleType</code>&nbsp;:&nbsp;[typeばバリデーションルールではない場合必須]バリデーションルールです</li>'.
            '  <li><code>condition</code>&nbsp;:&nbsp;[条件が必要なtypeの場合必須]バリデーションの条件です</li>'.
            '  <li><code>options</code>&nbsp;:&nbsp;バリデーションで使用する追加設定です</li>'.
            '  <li><code>flow</code>&nbsp;:&nbsp;特定の処理の時だけ有効にする場合に指定します</li>'.
            '  <li><code>key</code>&nbsp;:&nbsp;バリデーションキー名が必要な場合に指定します</li>'.
            '</ul>'.
            'tyoeは原則としてLaravelのバリデーションルールを指定しますが、validatorクラスに<code>getValidateTypeタイプ名</code>
             というメソッドを作成すると、そのメソッドを呼び出して条件を作成します
             また、ruleTypeを指定するとルール名がここで指定された名前になります
             uniqueバリデーションをしたい場合に、条件作成に別途<code>getValidateTypeLogin</code>というメソッドを作り
             type=>login, ruleType=>uniqueと指定することで、[\'unique:getValidateTypeLoginで作成された条件\']という形で
             バリデーションルールを構成できます
             optionsは<code>getValidateTypeタイプ名</code>に引数として渡されます。ルール作成に必要な追加設定を自由に構成します
             flowは配列で指定すると複数のflowに対応できます
             具体的な使い分けとしては、パスワードのバリデーションとして、
             [\'type\' => \'required\', \'flow\' => \'regist\'],
             [\'type\' => \'nullable\', \'flow\' => \'edit\'],
             と指定すれば、新規登録時は必須。更新時は任意となります
             keyに指定した内容は<code>form->要素キー->column</code>が自動的に補完されます',
        ],
    ],
])

        </p>
      </div>
      
    </div>

    <h4 class="margin-top-1">リスト</h4>
    <div class="section-block">
      <p>
        <code>list</code>キーの設定で一覧表示時にその要素をどう表示するかを設定します。
        一覧表示がない要素の場合、この設定は省略できます。<br>
        なお、ひとつの<code>list</code>キー設定に複数の項目の設定をすることも可能ですので、一覧に関する設定のみをまとめて
        要素記述することも可能です。<br>
        ただし、その場合は要素の補完が出来ないため必要なすべての設定値の記述が必要になります
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '自動補完', '内容（設定方法）'
    ],
    'body' => [
        [
            'title',
            '必須',
            '〇',
            'ヘッダタイトルです。指定がない場合、<code>form->title->name</code>で自動補完されます',
        ],
        [
            'column',
            '必須',
            '〇',
            '対応するデータのカラム名です。指定がない場合、<code>form->[項目キー]->column</code>で自動補完されます',
        ],
        [
            'type',
            '必須',
            '×',
            '以下のいずれかを指定'.
            '<ul class="no-decoration">'.
            '  <li><code>CONTROL_TYPE_TEXT</code>&nbsp;:&nbsp;登録されている値をそのままテキスト表示</li>'.
            '  <li><code>CONTROL_TYPE_SELECT</code>&nbsp;:&nbsp;登録されている値に紐づいたマスターデータで設定されている文字列で表示</li>'.
            '  <li><code>CONTROL_TYPE_DATE</code>&nbsp;:&nbsp;Y/m/dフォーマットで表示</li>'.
            '  <li><code>CONTROL_TYPE_DATETIME</code>&nbsp;:&nbsp;Y/m/d H:i:sフォーマットで表示</li>'.
            '  <li><code>CONTROL_TYPE_BOOLEAN</code>&nbsp;:&nbsp;〇&nbsp;&nbsp;or&nbsp;&nbsp;×</li>'.
            '  <li><code>CONTROL_TYPE_CONTROLLER</code>操作ボタンを表示</li>'.
            '</ul>',
        ],
        [
            'width',
            '必須',
            '×',
            'thの幅指定。thタグのwidth属性にそのまま指定されるので、<code>10%</code>のように指定します
             ブラウザがよきに計らうため指定の通りになるとは限りません',
        ],
        [
            'select',
            'typeにより
             必須',
            '〇',
            'typeが[CONTROL_TYPE_SELECT]の場合必須です。指定がない場合、<code>form->[項目キー]->select</code>で自動補完されます
             設定がなく、補完もできなかった場でもエラーにはなりません',
        ],
        [
            'format',
            '任意',
            '×',
            'CONTROL_TYPE_DATEあるいは、CONTROL_TYPE_DATETIMEの時、ここで任意のフォーマットを指定できます
             指定できるのはdateメソッドに対応したフォーマット文字列です',
        ],
        [
            'orderable',
            '任意',
            '×',
            'この項目での並び替えを有効にする場合はtrueを、無効にする場合はfalseを指定します。指定自体がない場合はfalseになります
             trueにした場合、並び替えのためのアイコンが表示され並び替えが出来るようになります
             対象のカラムによっては、Model側の実装が必要になる場合があります',
        ],
        [
            'orderColumn',
            '任意',
            '×',
            '並び替えに使用するカラム名が<code>column</code>の指定とは違うカラムの場合にここで設定します',
        ],
        [
            'detailLinkable',
            '任意',
            '×',
            'この値にtrueを指定すると、詳細画面へのリンクがつきます',
        ],
        [
            'ctrl',
            'typeにより
             必須',
            '×',
            'typeが[CONTROL_TYPE_CONTROLLER]の場合必須
             以下のいずれかを指定'.
            '<ul class="no-decoration">'.
            '  <li><code>CTRL_DETAIL</code>&nbsp;:&nbsp;詳細画面へのリンクアイコン</li>'.
            '  <li><code>CTRL_EDIT</code>&nbsp;:&nbsp;編集画面へのリンクアイコン</li>'.
            '  <li><code>CTRL_DELETE</code>&nbsp;:&nbsp;削除モーダル表示へのリンクアイコン</li>'.
//            '  <li><code>CTRL_EDITONLIST</code>&nbsp;:&nbsp;</li>'.
//            '  <li><code>CTRL_COPY</code>&nbsp;:&nbsp;</li>'.
//            '  <li><code>CTRL_RESTORE</code></li>'.
//            '  <li><code>CTRL_PREVIEW</code></li>'.
//            '  <li><code>CTRL_CHANGE_ORDER</code></li>'.
            '</ul>',
        ],
    ],
])

      </p>
    </div>

    
  </div>

  <h3 id="hack-2">CtrlSupporterの設定</h3>
  <div class="section-block">
    <p>
      CtrlSupporterクラスは初期化時に各種設定を行うことで挙動をコントロールできます<br>
      また、各種メソッドをbindし、挙動をコントロールすることができます<br>
      CtrlSupporterクラスを使うことで管理画面のような決まった構成の画面を簡単に実装できるようになっています
    </p>
    <h4>初期化時設定項目</h4>
    <div class="section-block">
      <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '内容（設定方法）'
    ],
    'body' => [
        [
            'templatePrefix',
            '任意',
            'テンプレートファイルのプレフィックスを指定します
             画面毎にデフォルトのテンプレート名が決まっており、ここで指定した値はそのデフォルト名のプレフィックスとして使用されます
             デフォルトのテンプレート名はルーティングで設定したname値になっており、<code>list,detail,form,confirm</code>などになります
             postの場合、本システムでは<code>p-</code>がつきますが、これは削除して名前判定をします
             例）ここで、<code>hoge.foo.</code>と指定すると、一覧画面アクセス時には<code>resources/views/hoge/foo/list.blade.php</code>
             をテンプレートとして使用します
             ver1.2.x以降は<code>protected function getCtrlSuppoterTemplatePrefix()</code>の戻り値が使用されますので、
             このメソッドをオーバーライドすることで操作することができます。',
        ],
        [
            'templates',
            '任意',
            '個別にテンプレートの指定が必要な場合に設定します
             templatePrefixの値は補完されません
             一覧画面のみ<code>resources/views/share-list.blade.php</code>というテンプレートファイルを使いたいといった場合、
             <code>[ \'list\' => \'share-list\']</code>と指定できます
             また、この設定はクロージャ―を指定することも可能です
             配列のキー名は「ルーティングで設定したname値」となります
             ver1.2.x以降はメンバ変数<code>viewTemplates</code>で定義することができます',
        ],
        [
            'model',
            '必須',
            '<code>Illuminate\Database\Eloquent\Model</code>を継承したモデルクラスの実体を指定します
             本システムでは、原則としてコンストラクタで自動注入したモデルクラスをそのまま設定します',
        ],
        [
            'validator',
            '必須',
            '<code>Azuki\App\Http\Validation\Validator</code>を継承したバリデーションクラスの実体を指定します
             本システムでは、原則としてコンストラクタで自動注入したバリデーションクラスをそのまま設定します',
        ],
        [
            'order',
            '任意',
            '一覧でのデフォルトの並び順を指定します
             [\'order\' => \'カラム名1|カラム名2\', \'condition\' => [\'カラム名1\' => \'desc or asc\', \'カラム名2\' => \'desc or asc\']]
             上記のような構成で指定します。<code>order</code>で指定した順番にorderByを設定していきます
             複数の指定は「|」で区切ります。ascかdescかはconditin配列で指定します
             orderの指定がない場合のデフォルトはモデルクラスでの指定となり、<code>orderBy(\'id\', \'desc\')</code>を設定しています
             ver1.2.x以降はメンバ変数<code>viewOrder</code>で定義することができます',
        ],
        [
            'views',
            '必須',
            'ここで指定した設定は<code>view->share</code>に設定されます
             viewに渡したいパラメータを任意に設定できます
             本システムで使用している項目については別途記載します
             ver1.2.x以降は<code>protected function addCtrlSuppoterViews($view)</code>にてパラメータの追加・上書きが出来ます',
        ],
    ],
])
      </p>

      <h5>viewsの設定</h5>
      <div class="section-block">
        <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'キー', '必須・任意', '内容（設定方法）'
    ],
    'body' => [
        [
            'pageName',
            '必須',
            'ページ名の値です。システム的には必須ではないためテンプレート修正すれば任意となります
             ver1.2.x以降ではメンバ変数<code>pageNamePrefix</code>で定義できます',
        ],
        [
            'controller',
            '必須',
            'コントローラー名をケバブケースで指定します。URLに使用します
             ver1.2.x以降ではメンバ変数<code>controllerName</code>で定義できます',
        ],
        [
            'search_shortcut',
            '任意',
            'ショートタイプの検索フィールドを有効にする際に<code>true</code>を指定します
             ver1.2.x以降ではメンバ変数<code>searchShortcut</code>にてtrue/falseで操作できます',
        ],
        [
            'needDatetimePicker',
            '任意',
            'datetimeタイプのフォームを使用する際に<code>true</code>を指定する必要があります
             本設定をtrueにすることで、datetime-pickerのJS・CSSが読み込まれます
             ver1.2.x以降ではメンバ変数<code>needDatetimePicker</code>にてtrue/falseで操作できます',
        ],
        [
            'csvSettings',
            '任意',
            'CSVアップロード・ダウンロード機能を表示する際に指定します
             <code>[
                    \'ufType\' => \'csv1\',
                    \'kind\'   => \'sample_csv\',
             ]</code>
             <code>ufType</code>は<code>config.azuki.upload_file</code>で、<code>kind</code>は<code>config.azuki.execute_type</code>
             で定義している値を記載します
             ver1.2.x以降ではメンバ変数<code>csvSettings</code>で定義できます',
        ],
        [
            'ctrlCondition',
            '任意',
            '一覧に表示される編集ボタン・削除ボタンの表示をコントロールする設定です
             \'edit or delete\' => [ \'default\' => \'初期値（true or false）\', \'enable or disable\' => \'条件\' ]
             という形で指定します
             設定のパターンとしては、
             [ \'default\' => \'false\' ] // 常に非表示
             [ \'default\' => \'false\', \'enable\' => \'条件\' ] // 条件に合致した場合のみ表示
             [ \'default\' => \'true\', \'disable\' => \'条件\' ] // 条件に合致した場合のみ非表示
             になります。条件は配列で指定し、複数指定した場合はand判定されます
             条件として、[\'id\' => \'1\'] と指定した場合、idが1のものが対象となります
             条件は完全一致のみ指定できます。〇〇以上や〇〇を含むなどの指定はできません
             また、ここでの指定は表示条件のみですので、処理に対する禁則の実装は別途行う必要があります
             ver1.2.x以降ではメンバ変数<code>ctrlCondition</code>で定義できます',
        ],
    ],
])
        </p>
      </div>
    </div>

    <h4>オーバーライドメソッドについて</h4>
    <div class="section-block">
      <p>
        任意のメソッドをbindすることができます。<br>
        アクション実装にてbindした任意のメソッドを呼び出して動作を拡張することができます<br>
        また、既存のbindメソッドをオーバーライドして挙動を変えることもできます。以下に主なbindメソッドを記載します<br>
        bindメソッドは上書きで後勝ちになります。
      </p>

      <p>
@include($vendor.'azuki.001-parts-table', [
    'head' => [
        'メソッド名', '引数', '戻り値', 'デフォルトの処理', '内容（設定方法）'
    ],
    'body' => [
        [
            'getRouteForIndexRedirect',
            'void',
            '文字列
             (routeメソッドの引数として使用できるURLの名前)',
            '<code>return $this->getNameListRoute();</code>',
            'インデックスアクセス時のリダイレクト先をルート名を返す処理
             traitのインデックスアクションにて呼び出される<code>getSubstituteIndexRoute</code>メソッド内で呼び出されています
             <code>getSubstituteIndexRoute</code>メソッドの第一引数に指定がない場合、このメソッドの戻り値が使用されます
             デフォルトは<code>return $this->getNameListRoute();</code>であり、一覧画面の名前を返すようになっています',
        ],
        [
            'shareAsaignList',
            'void',
            'void',
            '<code>view()->share(\'order\', $this->getOrderCondition());</code>',
            '一覧表示時のアサインの追加処理
             viewメソッド呼び出しの直前で呼び出されます。並び順に関する設定をアサインしています
             オーバーライドする際にはこの処理もオーバーライド先で実装しておく必要があります',
        ],
        [
            'shareAsaignForm',
            'void',
            'void',
            '<code>view()->share(\'forms\', $this->forms)</code>',
            '登録編集時のアサインの追加処理
             viewメソッド呼び出しの直前で呼び出されます。フォームに関する設定をアサインしています
             オーバーライドする際にはこの処理もオーバーライド先で実装しておく必要があります',
        ],
        [
            'shareAsaignConfirm',
            'void',
            'void',
            '<code>view()->share(\'forms\', $this->forms)</code>',
            '確認時のアサインの追加処理
             viewメソッド呼び出しの直前で呼び出されます。フォームに関する設定をアサインしています
             オーバーライドする際にはこの処理もオーバーライド先で実装しておく必要があります',
        ],
        [
            'shareAsaignDetail',
            'void',
            'void',
            '<code>view()->share(\'forms\', $this->forms)</code>',
            '詳細時のアサインの追加処理
             viewメソッド呼び出しの直前で呼び出されます。フォームに関する設定をアサインしています
             オーバーライドする際にはこの処理もオーバーライド先で実装しておく必要があります',
        ],
        [
            'getRedirectNameFromComplete',
            'void',
            '文字列
             (routeメソッドの引数として使用できるURLの名前)',
            '<code>return $this->getNameListRoute();</code>',
            '登録・更新時の戻りURLを返す処理
             登録・更新以外に削除などのアクション発生時に戻る先もこのメソッドの実装で変更できます
             デフォルトは一覧画面になるように実装されています',
        ],
        [
            'getNameListRoute',
            'void',
            '文字列
             (routeメソッドの引数として使用できるURLの名前)',
            '<code>return str_replace( strrchr( $this->urlName, \'.\' ) , \'.list\', $this->urlName );</code>',
            '一覧画面のURLを返す処理
             一覧画面に紐づける名前がデフォルトの<code>xxxxx.list</code>形式になっていない場合
             このメソッドをオーバーライドして変更することができます',
        ],
        [
            'getCanonicalUrlName',
            'void',
            '文字列
             (routeメソッドの引数として使用できるURLの名前)',
            '<code>return str_replace( \'.p-\', \'.\', $this->urlName );</code>',
            '本システムではPOSTの場合、Routeで定義する各URLに紐づく名前に対して重複を回避するため<code>p-</code>を付けるようになっています
             GETとPSOTでURL自体は変わらないため一意なURLに対応した名前を取得する手段として本メソッドで統合しています
             テンプレートファイルの判定やURL単位でのセッションの管理の際に使用しています',
        ],
        [
            'beforeDisplayEditForm',
            '$id:
             選択されているデータのID
             $post:
             ポストされた値',
            'void',
            'void',
            '編集画面表示直前でアクションメソッドより呼び出されます
             このタイミングで追加処理が必要な場合のためのフックポイントです',
        ],
        [
            'beforeDisplayConfirm',
            '$id:
             選択されているデータのID
             $post:
             ポストされた値',
            'void',
            'void',
            '確認画面表示直前でアクションメソッドより呼び出されます
             このタイミングで追加処理が必要な場合のためのフックポイントです',
        ],
        [
            'beforeRegistData',
            '$data:
             登録・更新しようとしているデータ
             $flow:
             登録か更新か',
            '$data:
             登録・更新するデータ',
            '<code>return $data;</code>',
            'データの登録・更新処理の直前で呼び出されます
             画面上は操作させないデータを追加で設定するなどの処理が必要な場合にここに実装することができます',
        ],
    ],
])
      </p>
      <p>
        CtrlSuppoterはコントローラー毎にインスタンスを持つ構成としているため、コントローラー毎にbindさせるメソッドの実装内容を
        変更することができます<br>
        コントローラーは機能単位で持つことを基本的な構成として設計していますので、機能単位で変更できることになります
      </p>
    </div>

  </div>

  <h3 id="hack-3">バリデーションについて</h3>
  <div class="section-block">
    <p>
    </p>
  </div>


  <h3 id="hack-4">モデルについて</h3>
  <div class="section-block">
    <p>
    </p>
  </div>


  <h3 id="hack-5">メール送信について</h3>
  <div class="section-block">
    <p>
    </p>
  </div>


  <h3 id="hack-10">ハックする</h3>
  <div class="section-block">
    <p>
    </p>
  </div>

</div>
