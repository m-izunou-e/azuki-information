jQuery(function($){
    var toTopBtn = $('div.to-page-top');
    var scLimit = window.innerHeight / 2;
    var toTopIsDisplay = false;

    $(window).scroll(function() {
        if(!toTopIsDisplay && $(this).scrollTop() >= scLimit) {
            toTopIsDisplay = true;
            toTopBtn.fadeIn(500, function(){
            });
        }
        if(toTopIsDisplay && $(this).scrollTop() < scLimit) {
            toTopIsDisplay = false;
            toTopBtn.fadeOut(500, function(){
            });
        }
    });
    
    $('button#toc-button').click(function(){
        $(this).prop('disabled', true);
        $('div#azuki-table-of-contents').animate({right: 0}, 500, 'linear', function(){
            $('button#close-toc').prop('disabled', false);
        });
    });
    
    $('button#close-toc').click(function(){
        $(this).prop('disabled', true);
        $('div#azuki-table-of-contents').animate({right: '-350px'}, 500, 'linear', function(){
            $('button#toc-button').prop('disabled', false);
        });
    });

});
