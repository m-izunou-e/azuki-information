<?php
return [
    'namespace' => '\AzukiInformation\App\Contracts\Http\Controllers',
    // /～に関するログインが不要なURLのルーティング
    'list' => [
        [   // サイトトップページ
            'url'        => '/azuki/{kind?}',
            'method'     => 'get',
            'name'       => 'index',
            'middleware' => [],
            'uses'       => 'Common\\AzukiController@getIndex',
            'where'      => ['kind' => 'about|install|setup|system|develop|hack|milestone|change-log'],
        ],
    ],
];
