<?php
namespace AzukiInformation;

use Azuki\App\Support\ServiceProvider as AzukiServiceProvider;
use Azuki\App\Services\RouteControll;

/**
 *
 *
 *
 */
class ServiceProvider extends AzukiServiceProvider
{
    /**
     * azukiで使用している設定ファイルのリスト
     *
     */
    protected $configList = [
        'azuki.addon' => ['isPublish' => false, 'merge' => self::CONF_MERGE_TYPE_NRL],
    ];

    /**
     * azukiの基本システムを構成しているコントローラ、モデル、バリデーター
     * のリスト
     * ここで定義されているキー名に一意なプレフィックスをつけた名前で各クラスを
     * コンテナに登録します
     *
     */
    protected $baseServiceClassList = [
        'controller' => [
            'common.azuki' => 'Common\\AzukiController',
        ],
    ];
    
    /**
     *
     *
     */
    protected $classKindKeys = [
        'controller',
    ];
    
    /**
     *
     *
     */
    protected $publishVendorName = 'azuki';
    
    /**
     *
     */
    protected $publishList = [
        ['src' => '/img', 'dst' => '/img'],
        ['src' => '/css', 'dst' => '/css'],
        ['src' => '/js',  'dst' => '/js'],
    ];
    
    /**
     *
     *
     */
    protected function getVendorPath()
    {
        return __DIR__.'/../';
    }
    
    /**
     *
     *
     */
    protected function registerConfigFiles()
    {
        parent::registerConfigFiles();

        if (!$this->app->configurationIsCached()) {
            $azukiInformation = env( 'AZUKI_INFORMATION', true);
            $this->app['config']->set(
                'azuki.standard.azuki_information',
                $azukiInformation
            );
            // urlの設定を追加
            $this->app['config']->set(
                'azuki.standard.url', array_merge(
                    $this->app['config']->get('azuki.standard.url', []),
                    ['/azuki' => $azukiInformation === true  ? 'enabled' : 'disabled',]
            ));
        }
    }
    
    /**
     *
     *
     *
     */
    protected function getRouteControllerInstance($conf)
    {
        return  new RouteControll($conf);
    }
    
    /**
     * function setPublishes
     *
     * vendor:publishでパブリッシュするファイルを設定する
     *
     */
    protected function setPublishes()
    {
        $this->setPublishView();
        $this->setPublishPublic();
    }
    
    /**
     *
     *
     */
    protected function addVendorRouting()
    {
        return $this->app['config']->get('azuki.standard.routing');
    }
    
    /**
     *
     *
     */
    protected function routeFileList()
    {
        return [
            'azuki.web',
        ];
    }
}
