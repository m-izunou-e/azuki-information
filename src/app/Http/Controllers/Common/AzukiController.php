<?php
namespace AzukiInformation\App\Http\Controllers\Common;

/**
 * フロント画面トップページを扱うコントローラ
 * 
 *
 * @copyright Copyright 2018 Azuki Project.
 * @author    Yoshitaka Mizunoue <yoshitaka.mizunoue@gmail.com>
 */

use Azuki\App\Http\Controllers\Common\BaseController;

/**
 * class IndexController
 *
 */
class AzukiController extends BaseController
{
    /**
     *
     * ['kind' => 'about|install|setup|system|develop|hack|milestone|change-log']
     */
    protected $page = [
        // {{-- h2 Azukiシステムとは  id="about-azuki" --}}
        'about' => [
            'title' => 'Azukiシステムとは',
            'tpl'   => 'about',
        ],
        // {{-- h2 Azukiシステムのインストール  id="about-install" --}}
        'install' => [
            'title' => 'Azukiシステムのインストール',
            'tpl'   => ['install', 'install-laravel', 'install-package', 'initialize'],
            // {{-- h3 Laravel6をインストールする  id="install-laravel" --}}
            // {{-- h3 Azukiパッケージをインストールする  id="install-azuki" --}}
            // {{-- h3 Azukiパッケージの初期設定を行う   id="init-azuki" --}}
            'link' => [
                ['anchor' => 'install-laravel', 'title' => 'Laravelのインストール'],
                ['anchor' => 'install-azuki',   'title' => 'Azukiパッケージのインストール'],
                ['anchor' => 'init-azuki',      'title' => 'Azukiパッケージの初期設定'],
            ],
        ],
        // {{-- h2 Azukiシステムの基本機能  id="base-system-azuki" --}}
        'system' => [
            'title' => 'Azukiシステムの基本機能',
            'tpl'   => 'base-system',
        ],
        // {{-- h2 Azukiシステムを使った開発を行う  id="devlop-use-azuki" --}}
        'develop' => [
            'title' => 'Azukiシステムを使った開発',
            'tpl'   => 'develop',
        ],
        // {{-- h2 Azukiシステムをハックする  id="hack" --}}
        'hack' => [
            'title' => 'Azukiシステムをハックする',
            'tpl'   => 'hack',
        ],
        // {{-- h2 今後の予定  id="milestone" --}}
        'milestone' => [
            'title' => '今後の予定',
            'tpl'   => 'milestone',
        ],
        // {{-- h2 更新履歴  id="change-log" --}}
        'change-log' => [
            'title' => '更新履歴',
            'tpl'   => 'change-log',
        ],
    ];
    
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * function getIndex
     *
     * @return void
     */
    public function getIndex($kind = 'about')
    {
        $vendor = 'azuki-information::';
        return view($vendor.'azuki.index', [
            'vendor'    => $vendor,
            'pageKind'  => $kind,
            'pageParts' => $this->getPageParts($kind),
            'pageList'  => $this->getPageList(),
        ]);
    }
    
    /**
     *
     *
     */
    protected function getPageList()
    {
        return $this->page;
    }
    
    /**
     *
     *
     */
    protected function getPageParts($kind)
    {
        $parts = [];
        foreach($this->page as $param => $conf) {
            if($kind == 'all' || $kind == $param) {
                if(!isset($conf['tpl'])) {
                    continue;
                }
                $tpls = is_array($conf['tpl']) ? $conf['tpl'] : [$conf['tpl']];
                foreach($tpls as $tpl) {
                    $parts[] = $tpl;
                }
            }
        }
        return $parts;
    }
}
